﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSCsize : MonoBehaviour
{
    //public float degreesPerSecond = 2.0f;
    SphereCollider earth_sc;

    private void Start()
    {
        earth_sc = this.GetComponent<SphereCollider>();
        earth_sc.radius = GetSize().x;
    }

    Vector3 GetSize()
    {
        Mesh earth_mesh = this.GetComponentInChildren<MeshFilter>().mesh;
        Vector3 size = earth_mesh.bounds.size / 2;
        return size;
        //print("EARTH SIZE: " + size);
    }

    void Update()
    {
        //transform.Rotate(0, degreesPerSecond * Time.deltaTime, 0);
    }
}
