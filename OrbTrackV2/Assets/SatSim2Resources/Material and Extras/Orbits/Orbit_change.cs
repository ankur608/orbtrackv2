﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit_change : MonoBehaviour
{
    public float thruster;
    Rigidbody this_sat;
    // Start is called before the first frame update
    void Start()
    {
        this_sat = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 thrust_direction = this_sat.velocity.normalized;
            Vector3 force = thrust_direction * thruster;
            this_sat.AddForce(force);
        }
    }
}
