﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground_track : MonoBehaviour
{
    public GameObject Sat;
    public float cam_distance;
    private void Start()
    {
        Vector3 sat_pos = Sat.transform.position.normalized;
        Vector3 radial_look_direction = -sat_pos;
        transform.position = (new Vector3(sat_pos.x, sat_pos.y, sat_pos.z)) * cam_distance;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 sat_pos = Sat.transform.position.normalized;
        transform.position = (new Vector3(sat_pos.x, sat_pos.y, sat_pos.z)) * cam_distance;
        Vector3 cam_look = transform.forward;
        Vector3 radial_look_direction = -sat_pos;
        transform.forward = new Vector3(radial_look_direction.x, radial_look_direction.y, radial_look_direction.z);
    }
}
