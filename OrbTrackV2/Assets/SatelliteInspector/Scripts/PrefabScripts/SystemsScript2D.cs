using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace SatelliteInspector.Scripts.PrefabScripts
{
    public class SystemsScript2D : MonoBehaviour
    {
        //SatelliteInspector
        public SatInspectorScript2D SIS;

        /// <summary>
        /// Count of the data files
        /// </summary>
        public int Count;
   
        /// <summary>
        /// Name of the all data files
        /// </summary>
        public string[] NamesAllSystems;

        /// <summary>
        /// Selected system (in inspector)
        /// </summary>
        public int PopupSelectedIndex;

        /// <summary>
        /// Empty system for create
        /// </summary>
        public GameObject EmtySystem;

        /// <summary>
        /// Index of the selected system
        /// </summary>
        public int indexFromList;

        /// <summary>
        /// Get List of created systems
        /// </summary>
        public List<GameObject> ListCreatedSystems
        {
            get
            {
                List <GameObject> list = new List<GameObject>();
                for (int i = 0; i < GetComponentsInChildren<OneSystemScript2D>().Length; i++)
                {
                    list.Add(GetComponentsInChildren<OneSystemScript2D>()[i].gameObject);
                }
                return list;
            }
        }

        /// <summary>
        /// Array of the child systems
        /// </summary>
        private OneSystemScript2D[] OneSystems => GetComponentsInChildren<OneSystemScript2D>();

        /// <summary>
        /// Selected single system
        /// </summary>
        public OneSystemScript2D SelectedSystem
        {
            get
            {
                return ListCreatedSystems.Count != 0 ? ListCreatedSystems[indexFromList].GetComponent<OneSystemScript2D>() : null;
            }
        }

        /// <summary>
        /// Get array of the systems name
        /// </summary>
        public string[] NamesSystem
        {
            get
            {
                string[] names = new string[OneSystems.Length];
                for (int i = 0; i < OneSystems.Length; i++)
                {
                    names[i] = OneSystems[i].name;
                }
                return names;
            }
        }

        /// <summary>
        /// Get all names of the system from data directory
        /// </summary>
        public void GetNamesOfSystems() 
        {        
            string[] dirsDataFiles = Directory.GetFiles(Path.Combine(/*Directory.GetCurrentDirectory() + */SIS.DataFolder),"*.txt");
            Count = dirsDataFiles.Length;
            NamesAllSystems = new string[Count];
            for (int i = 0; i < NamesAllSystems.Length; i++)
            {
                NamesAllSystems[i] = Path.GetFileNameWithoutExtension(dirsDataFiles[i]);
            }
        }
    }
}
