using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeoMag8: MonoBehaviour
{
	public GameObject Earth;
	Rigidbody node_id75;
	int frame = 0;
	Vector3 sat_pos;
	double lat;
	double lon;
	double alt;
	public Vector3 mag_dir2;
	public Vector3 magnetometer;

	double longitude()
	{
		sat_pos = transform.position;
		double l = (double)(Math.Atan2(sat_pos.x, sat_pos.z) * (180 / Math.PI));
		double rot_earth = node_id75.rotation.eulerAngles.y;
		l = l - rot_earth;  // compute the azimuth angle with respect to earth axes

		//print("pos: " + pos.ToString() + " long: " + l.ToString());

		return l;
	}

	double latitude()
	{
		sat_pos = transform.position;
		double edist = Math.Sqrt(sat_pos.x * sat_pos.x + sat_pos.z * sat_pos.z);
		double lat = (double)(Math.Atan2(sat_pos.y, edist) * (180 / Math.PI));
		//print("pos: " + pos.ToString() + " lat: " + lat.ToString());
		return lat;
	}

	double altitude()
	{
		sat_pos = transform.position;
		double sat_distance = sat_pos.magnitude;
		double alt = sat_distance - GetSize().x;
		return alt;

	}

	Vector3 GetSize()
	{
		Mesh earth_mesh = Earth.GetComponentInChildren<MeshFilter>().mesh;
		Vector3 size = earth_mesh.bounds.size / 2;
		return size;
	}


	double[] calc_mag()
	{

		lat = deg_to_rad(latitude());
		lon = deg_to_rad(longitude());
		alt = altitude(); // altitude in meters
		/*print("Lat: " + lat);
		print("Longitude: " + lon);
		print("Altitude: " + alt + " km");*/

		int yy = 22;
		int mm = 11;
		int dd = 29;

		long date = yymmdd_to_julian_days(yy, mm, dd);
		//print("Date: " + date);

		int model = 2015;
		double[] field = new double[6];
		double declination = SGMagVar(lat, lon, alt, date, model, field);
		double declination_deg = rad_to_deg(declination);
		/*print("Declination: " + declination);
		print("Declination angle: "+ declination_deg);*/
		//print("Field values: " + field[0] + ", " + field[1] + ", " + field[2] + ", " + field[3] + "," + field[4] + ", " + field[5]);

		return field;
	}

	void DrawMag(double[] mag_field)
	{
		sat_pos = transform.position;
		Vector3 target_look_dir = -sat_pos.normalized;
		Vector3 actual_look_dir = new Vector3(0, -1, 0);

		Vector3 satpos_dir = new Vector3(-sat_pos.x, -sat_pos.y, -sat_pos.z).normalized;
		Quaternion fromtorotation = Quaternion.FromToRotation(startdir, satpos_dir);
		mag_dir2 = fromtorotation * mag_dir;

		Quaternion pointing_correction = Quaternion.FromToRotation(actual_look_dir, target_look_dir);
		/* Debug.DrawLine(sat_pos, sat_pos + actual_look_dir * 4000, Color.blue, 500.0f);
		Debug.DrawLine(sat_pos, sat_pos + target_look_dir * 4000, Color.green, 500.0f);*/
		Quaternion actual_dir_to_mag = Quaternion.FromToRotation(actual_look_dir, new Vector3(0, 0, 1)); // rotation from global axes to sat-perspective axes

		magnetometer = actual_dir_to_mag * mag_dir2;

		Vector3 base_mag_dir = new Vector3(0, -1, 0);
		Quaternion target_magnetometer_q = Quaternion.FromToRotation(target_look_dir, mag_dir2);
		Quaternion actual_magnetometer_q = Quaternion.FromToRotation(actual_look_dir, mag_dir2);
		Vector3 rand_pos = new Vector3(12000, 12000, 0);

		//Debug.DrawLine(rand_pos, rand_pos + pointing_correction * base_mag_dir * 2000, Color.green, 0.2f);
		//Debug.DrawLine(rand_pos, rand_pos + target_magnetometer_q*base_mag_dir * 2000, Color.green, 0.2f);
		//Debug.DrawLine(rand_pos, rand_pos + actual_magnetometer_q*base_mag_dir * 2000, Color.blue, 0.2f);
		Debug.DrawLine(sat_pos, sat_pos + magnetometer.normalized * 2, Color.red, 1.0f);
		/*Debug.DrawLine(rand_pos, rand_pos + mag_dir.normalized * 2000, Color.green, 0.2f);
		Debug.DrawLine(rand_pos, rand_pos + magnetometer.normalized * 2000, Color.blue, 0.2f);*/

		//print("Pointing correction: " + pointing_correction.eulerAngles);
		//print("magnetometer: " + magnetometer);

		Debug.DrawLine(sat_pos, sat_pos + mag_dir2.normalized * 1, Color.green, 10.0f);
		Debug.DrawLine(sat_pos, sat_pos + pointing_correction * base_mag_dir * 2, Color.blue, 0.2f);

	}

	Vector3 mag_dir;  // Magnetic field when looking radially into the earth
	Vector3 startdir = new Vector3(0, 0, 1); // direction of inward look
	Vector3 startmagdir = new Vector3(1, 0, 0); // direction of east magnetic field at equator

	private void Start()
	{
		Earth = GameObject.Find("node_id75");
		node_id75 = Earth.GetComponent<Rigidbody>();
		double[] mag_field = calc_mag();
		mag_dir = (new Vector3((float)mag_field[4], (float)mag_field[3], (float)mag_field[5])); // X = north intensity , Y = East intensity, Z = vertical intensity
		DrawMag(mag_field);
	}

	private void FixedUpdate()
	{

		if (frame % 10 == 0)
		{
			//double latt = latitude();
			//double longt = longitude();
			double[] mag_field = calc_mag();
			mag_dir = (new Vector3((float)mag_field[4], (float)mag_field[3], (float)mag_field[5])); // X = north intensity , Y = East intensity, Z = vertical intensity
			DrawMag(mag_field);
		}

		frame += 1;
		frame = frame % 10000;
	}

	#region statics
	//const double a = 6378.16;	/* major radius (km) IAU66 ellipsoid */

	const double a = 6378.137; // iz WMM 2015 Calculator.xlsx sa www.ngdc.noaa.gov/geomag/WMM/DoDWMM.shtml 
							   //const double f = 1.0 / 298.25;	/* inverse flattening IAU66 ellipsoid */
	const double f = 1.0 / 298.2572235630; // iz WMM 2015 Calculator.xlsx sa www.ngdc.noaa.gov/geomag/WMM/DoDWMM.shtml 
										   //const double b = 6378.16 * (1.0 - 1.0 / 298.25);
	/* minor radius b=a*(1-f) */
	const double b = 6356.75231424518; // iz WMM 2015 Calculator.xlsx sa www.ngdc.noaa.gov/geomag/WMM/DoDWMM.shtml
	const double r_0 = 6371.2;  /* "mean radius" for spherical harmonic expansion */

	//************************************************************************
	// WMM Gausovi koeficijenti za 2015 su dodati rucno iz WMM 2015 Calculator.xlsx sa www.ngdc.noaa.gov/geomag/WMM/DoDWMM.shtml 
	// vaze od 01.01.2015 do 31.12.2019.

	static double[,] gnm_wmm2015 = new double[13, 13] {
			{0,0,0,0,0,0,0,0,0,0,0,0,0},
			{-29438.5, -1501.1, 0,0,0,0,0,0,0,0,0,0,0},
			{-2445.3, 3012.5, 1676.6, 0,0,0,0,0,0,0,0,0,0},
			{1351.1, -2352.3, 1225.6, 581.9, 0,0,0,0,0,0,0,0,0},
			{907.2, 813.7, 120.3, -335, 70.3, 0,0,0,0,0,0,0,0},
			{-232.6, 360.1, 192.4, -141, -157.4, 4.3, 0,0,0,0,0,0,0},
			{69.5, 67.4, 72.8, -129.8, -29, 13.2, -70.9, 0,0,0,0,0,0},
			{81.6, -76.1, -6.8, 51.9, 15, 9.3, -2.8, 6.7, 0,0,0,0,0},
			{24, 8.6, -16.9, -3.2, -20.6, 13.3, 11.7, -16, -2, 0,0,0,0},
			{5.4, 8.8, 3.1, -3.1, 0.6, -13.3, -0.1, 8.7, -9.1, -10.5, 0,0,0},
			{-1.9, -6.5, 0.2, 0.6, -0.6, 1.7, -0.7, 2.1, 2.3, -1.8, -3.6, 0,0},
			{3.1, -1.5, -2.3, 2.1, -0.9, 0.6, -0.7, 0.2, 1.7, -0.2, 0.4, 3.5, 0},
			{-2, -0.3, 0.4, 1.3, -0.9, 0.9, 0.1, 0.5, -0.4, -0.4, 0.2, -0.9, 0}
		};

	static double[,] hnm_wmm2015 = new double[13, 13]{
			{0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0, 4796.2, 0,0,0,0,0,0,0,0,0,0,0},
			{0, -2845.6, -642, 0,0,0,0,0,0,0,0,0,0},
			{0, -115.3, 245, -538.3, 0,0,0,0,0,0,0,0,0},
			{0, 283.4, -188.6, 180.9, -329.5, 0,0,0,0,0,0,0,0},
			{0, 47.4, 196.9, -119.4, 16.1, 100.1, 0,0,0,0,0,0,0},
			{0, -20.7, 33.2, 58.8, -66.5, 7.3, 62.5, 0,0,0,0,0,0},
			{0, -54.1, -19.4, 5.6, 24.4, 3.3, -27.5, -2.3, 0,0,0,0,0},
			{0, 10.2, -18.1, 13.2, -14.6, 16.2, 5.7, -9.1, 2.2, 0,0,0,0},
			{0, -21.6, 10.8, 11.7, -6.8, -6.9, 7.8, 1, -3.9, 8.5, 0,0,0},
			{0, 3.3, -0.3, 4.6, 4.4, -7.9, -0.6, -4.1, -2.8, -1.1, -8.7, 0,0},
			{0, -0.1, 2.1, -0.7, -1.1, 0.7, -0.2, -2.1, -1.5, -2.5, -2, -2.3, 0},
			{0, -1, 0.5, 1.8, -2.2, 0.3, 0.7, -0.1, 0.3, 0.2, -0.9, -0.2, 0.7}
		};

	static double[,] gtnm_wmm2015 = new double[13, 13]{
			{0,0,0,0,0,0,0,0,0,0,0,0,0},
			{10.7, 17.9, 0,0,0,0,0,0,0,0,0,0,0},
			{-8.6, -3.3, 2.4, 0,0,0,0,0,0,0,0,0,0},
			{3.1, -6.2, -0.4, -10.4, 0,0,0,0,0,0,0,0,0},
			{-0.4, 0.8, -9.2, 4, -4.2, 0,0,0,0,0,0,0,0},
			{-0.2, 0.1, -1.4, 0, 1.3, 3.8, 0,0,0,0,0,0,0},
			{-0.5, -0.2, -0.6, 2.4, -1.1, 0.3, 1.5, 0,0,0,0,0,0},
			{0.2, -0.2, -0.4, 1.3, 0.2, -0.4, -0.9, 0.3, 0,0,0,0,0},
			{0, 0.1, -0.5, 0.5, -0.2, 0.4, 0.2, -0.4, 0.3, 0,0,0,0},
			{0, -0.1, -0.1, 0.4, -0.5, -0.2, 0.1, 0, -0.2, -0.1, 0,0,0},
			{0, 0, -0.1, 0.3, -0.1, -0.1, -0.1, 0, -0.2, -0.1, -0.2, 0,0},
			{0, 0, -0.1, 0.1, 0,0,0,0,0,0, -0.1, -0.1, 0},
			{0.1, 0, 0, 0.1, -0.1, 0, 0.1, 0,0,0,0,0,0}
		};

	static double[,] htnm_wmm2015 = new double[13, 13]{
			{0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0, -26.8, 0,0,0,0,0,0,0,0,0,0,0},
			{0, -27.1, -13.3, 0,0,0,0,0,0,0,0,0,0},
			{0, 8.4, -0.4, 2.3, 0,0,0,0,0,0,0,0,0},
			{0, -0.6, 5.3, 3, -5.3, 0,0,0,0,0,0,0,0},
			{0, 0.4, 1.6, -1.1, 3.3, 0.1, 0,0,0,0,0,0,0},
			{0, 0, -2.2, -0.7, 0.1, 1, 1.3, 0,0,0,0,0,0},
			{0, 0.7, 0.5, -0.2, -0.1, -0.7, 0.1, 0.1, 0,0,0,0,0},
			{0, -0.3, 0.3, 0.3, 0.6, -0.1, -0.2, 0.3, 0, 0,0,0,0},
			{0, 0, -0.2, -0.1, -0.2, 0.1, 0, -0.2, 0.4, 0.3, 0,0,0},
			{0, 0.1, -0.1, 0, 0, -0.2, 0.1, -0.1, -0.2, 0.1, -0.1, 0,0},
			{0, 0, 0.1, 0, 0.1, 0, 0, 0.1, 0, -0.1, 0, -0.1, 0},
			{0, 0, 0, -0.1, 0,0,0,0,0,0,0,0,0}
		};

	#endregion

	const int nmax = 12;

	double[,] P = new double[13, 13];
	double[,] DP = new double[13, 13];
	double[,] gnm = new double[13, 13];
	double[,] hnm = new double[13, 13];
	double[] sm = new double[13];
	double[] cm = new double[13];

	static double[] root = new double[13];
	static double[,,] roots = new double[13, 13, 2];

	/* Convert date to Julian day    1950-2049 */
	public long yymmdd_to_julian_days(int yy, int mm, int dd)
	{
		long jd;

		yy = (yy < 50) ? (2000 + yy) : (1900 + yy);
		jd = dd - 32075L + 1461L * (yy + 4800L + (mm - 14) / 12) / 4;
		jd = jd + 367L * (mm - 2 - (mm - 14) / 12 * 12) / 12;
		jd = jd - 3 * ((yy + 4900L + (mm - 14) / 12) / 100) / 4;

		return (jd);
	}

	/* Convert degrees to radians */
	public double deg_to_rad(double deg) // dodah da bude public da bih preracunao lat,long iz deg u rad
	{
		return deg * Math.PI / 180.0;
	}

	/* Convert radians to degrees */
	public double rad_to_deg(double rad)
	{
		return rad * 180.0 / Math.PI;
	}

	public double cos(double d) { return Math.Cos(d); }
	public double sin(double d) { return Math.Sin(d); }
	public double sqrt(double d) { return Math.Sqrt(d); }
	public double max(double d, double e) { return Math.Max(d, e); }
	public int max(int d, int e) { return Math.Max(d, e); }
	public double min(double d, double e) { return Math.Min(d, e); }
	public double atan2(double d, double e) { return Math.Atan2(d, e); }
	public double pi { get { return Math.PI; } }
	static bool been_here = false;

	public unsafe double SGMagVar(double lat, double lon, double h, long dat, int model, double[] field)
	{
		/* output field B_r,B_th,B_phi,B_x,B_y,B_z */
		int n, m, nmaxl;
		double yearfrac, sr, r, theta, c, s, psi, fn, fn_0, B_r, B_theta, B_phi, X, Y, Z;
		double sinpsi, cospsi, inv_s;

		double sinlat = sin(lat);
		double coslat = cos(lat);

		/* convert to geocentric */
		sr = sqrt(a * a * coslat * coslat + b * b * sinlat * sinlat);
		/* sr is effective radius */
		theta = atan2(coslat * (h * sr + a * a), sinlat * (h * sr + b * b));

		/* theta is geocentric co-latitude */

		r = h * h + 2.0 * h * sr +
		(a * a * a * a - (a * a * a * a - b * b * b * b) * sinlat * sinlat) /
		(a * a - (a * a - b * b) * sinlat * sinlat);

		r = sqrt(r);

		/* r is geocentric radial distance */
		c = cos(theta);
		s = sin(theta);
		/* protect against zero divide at geographic poles */
		//inv_s =  1.0 / (s +(s == 0.0)*1.0e-8); 
		if (s == 0)
			inv_s = 1.0 / (s + 1 * 1.0e-8);
		else
			inv_s = 1.0 / (s + 0 * 1.0e-8);

		/*zero out arrays */
		for (n = 0; n <= nmax; n++)
		{
			for (m = 0; m <= n; m++)
			{
				P[n, m] = 0;
				DP[n, m] = 0;
			}
		}

		/* diagonal elements */
		P[0, 0] = 1;
		P[1, 1] = s;
		DP[0, 0] = 0;
		DP[1, 1] = c;
		P[1, 0] = c;
		DP[1, 0] = -s;

		/* these values will not change for subsequent function calls */
		if (!been_here)
		{
			for (n = 2; n <= nmax; n++)
			{
				root[n] = sqrt((2.0 * n - 1) / (2.0 * n));
			}

			for (m = 0; m <= nmax; m++)
			{
				double mm = m * m;
				for (n = max(m + 1, 2); n <= nmax; n++)
				{
					roots[m, n, 0] = sqrt((n - 1) * (n - 1) - mm);
					roots[m, n, 1] = 1.0 / sqrt(n * n - mm);
				}
			}
			been_here = true;
		}

		for (n = 2; n <= nmax; n++)
		{
			/*  double root = sqrt((2.0*n-1) / (2.0*n)); */
			P[n, n] = P[n - 1, n - 1] * s * root[n];
			DP[n, n] = (DP[n - 1, n - 1] * s + P[n - 1, n - 1] * c) * root[n];
		}

		/* lower triangle */
		for (m = 0; m <= nmax; m++)
		{
			/*  double mm = m*m;  */
			for (n = max(m + 1, 2); n <= nmax; n++)
			{
				/* double root1 = sqrt((n-1)*(n-1) - mm); */
				/* double root2 = 1.0 / sqrt( n*n - mm);  */
				P[n, m] = (P[n - 1, m] * c * (2.0 * n - 1) -
					   P[n - 2, m] * roots[m, n, 0]) * roots[m, n, 1];
				DP[n, m] = ((DP[n - 1, m] * c - P[n - 1, m] * s) *
						(2.0 * n - 1) - DP[n - 2, m] * roots[m, n, 0]) * roots[m, n, 1];
			}
		}

		/* compute gnm, hnm at dat */
		nmaxl = 12;  /* indeksiranje */
		switch (model)
		{
			case 2015:
				/* WMM2015 */
				yearfrac = (dat - yymmdd_to_julian_days(15, 1, 1)) / 365.25; //15 jer je 2015
				for (n = 1; n <= nmaxl; n++)
					for (m = 0; m <= nmaxl; m++)
					{
						gnm[n, m] = gnm_wmm2015[n, m] + yearfrac * gtnm_wmm2015[n, m];
						hnm[n, m] = hnm_wmm2015[n, m] + yearfrac * htnm_wmm2015[n, m];
					}
				break;

		}

		/* compute sm (sin(m lon) and cm (cos(m lon)) */
		for (m = 0; m <= nmaxl; m++)
		{
			sm[m] = sin(m * lon);
			cm[m] = cos(m * lon);
		}

		/* compute B fields */
		B_r = 0.0;
		B_theta = 0.0;
		B_phi = 0.0;
		fn_0 = r_0 / r;
		fn = fn_0 * fn_0;

		for (n = 1; n <= nmaxl; n++)
		{
			double c1_n = 0;
			double c2_n = 0;
			double c3_n = 0;
			for (m = 0; m <= n; m++)
			{
				double tmp = (gnm[n, m] * cm[m] + hnm[n, m] * sm[m]);
				c1_n += tmp * P[n, m];
				c2_n += tmp * DP[n, m];
				c3_n += m * (gnm[n, m] * sm[m] - hnm[n, m] * cm[m]) * P[n, m];
			}
			/* fn=pow(r_0/r,n+2.0);   */
			fn *= fn_0;
			B_r += (n + 1) * c1_n * fn;
			B_theta -= c2_n * fn;
			B_phi += c3_n * fn * inv_s;
		}



		/* Find geodetic field components: */
		psi = theta - (pi / 2.0 - lat);
		sinpsi = sin(psi);
		cospsi = cos(psi);
		X = -B_theta * cospsi - B_r * sinpsi;
		Y = B_phi;
		Z = B_theta * sinpsi - B_r * cospsi;

		field[0] = B_r;
		field[1] = B_theta;
		field[2] = B_phi;
		field[3] = X;
		field[4] = Y;
		field[5] = Z;   /* output fields */
		/* find variation in radians */
		/* return zero variation at magnetic pole X=Y=0. */
		/* E is positive */
		return (X != 0.0 || Y != 0.0) ? atan2(Y, X) : (double)0.0;

		//return 0.0;
	}
}
