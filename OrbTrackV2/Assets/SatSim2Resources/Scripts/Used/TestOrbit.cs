using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestOrbit : MonoBehaviour
{
    [SerializeField]
    float speed = 100f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var node_id75 = GameObject.Find("Earth");
        this.transform.RotateAround(node_id75.transform.position, Vector3.forward,
            speed * Time.deltaTime);

    }
}
