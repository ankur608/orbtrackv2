﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parent_child : MonoBehaviour
{
    public GameObject parent;
    // Start is called before the first frame update
    void Start()
    {
        transform.parent = parent.transform;
    }

}
