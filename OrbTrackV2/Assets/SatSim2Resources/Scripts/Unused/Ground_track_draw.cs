﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground_track_draw : MonoBehaviour
{
    public GameObject earth;
    public GameObject sat;
    private LineRenderer lineRenderer;
    private int pos_index = 0;
    float radius;
    int frame = 0;
    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        radius = GetSize().x + 50;
        //Vector3 sat_pos = sat.transform.position.normalized;
        //Vector3 startpos = (new Vector3(sat_pos.x, sat_pos.y, sat_pos.z)) * radius;
        //lineRenderer.SetPosition(pos_index, startpos);
        transform.position = new Vector3(0, 0, 0);
        //pos_index += 1;
    }

    Vector3 GetSize()
    {
        Mesh earth_mesh = earth.GetComponentInChildren<MeshFilter>().mesh;
        Vector3 size = earth_mesh.bounds.size / 2;
        return size;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (frame % 50 == 0)
        {
            Vector3 sat_pos = sat.transform.position.normalized;
            Quaternion rotation = transform.rotation;
            sat_pos = Quaternion.Inverse(rotation) * sat_pos;
            Vector3 linepos = (new Vector3(sat_pos.x, sat_pos.y, sat_pos.z)) * radius;
            lineRenderer.SetPosition(pos_index, linepos);
            pos_index += 1;
        }
        frame += 1;
        if (frame == 1000)
        {
            frame = 1;
        }
    }
}
