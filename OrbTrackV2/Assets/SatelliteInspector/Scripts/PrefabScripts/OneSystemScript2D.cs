using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace SatelliteInspector.Scripts.PrefabScripts
{
    public class OneSystemScript2D : MonoBehaviour
    {
        //SatInspectorScript
        public SatInspectorScript2D SIS;
   
        /// <summary>
        /// Is system checked?
        /// </summary>
        public bool isChecked;

        /// <summary>
        /// Satellite object - prefab
        /// </summary>
        public GameObject Satellite;

        /// <summary>
        /// List of created satellites
        /// </summary>
        public List<GameObject> SatellitesList;

        /// <summary>
        /// Path to satellites sustem data
        /// </summary>
        private string FileName => /*Directory.GetCurrentDirectory() + */SIS.DataFolder + name + ".txt";
   
        /// <summary>
        /// Text from file
        /// </summary>
        private string[] Lines => File.ReadAllLines(Path.Combine(FileName));

        /// <summary>
        /// Count sats in data file
        /// </summary>
        public int CountSats => Lines.Length / 3;

        /// <summary>
        /// Array of satellites data
        /// </summary>
        public string[][] MatrixTLE;

        /// <summary>
        /// Load satellite data matrix
        /// </summary>
        public void LoadMatrix()
        {
            MatrixTLE = new string[CountSats][];
            for (int i = 0; i < MatrixTLE.Length; i++)
            {
                MatrixTLE[i] = new string[3];
            }

            //create Nx3 array from TLE data
            for (int i = 0; i < MatrixTLE.Length; i++)
            {
                if (!string.IsNullOrWhiteSpace(Lines[i * 3]))
                {
                    MatrixTLE[i][0] = Lines[i * 3];
                }
                if (!string.IsNullOrWhiteSpace(Lines[i * 3 + 1]))
                {
                    MatrixTLE[i][1] = Lines[i * 3 + 1];
                }
                if (!string.IsNullOrWhiteSpace(Lines[i * 3 + 2]))
                {
                    MatrixTLE[i][2] = Lines[i * 3 + 2];
                }
            }
        }

    }
}