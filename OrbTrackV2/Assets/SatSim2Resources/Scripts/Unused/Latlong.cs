﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Latlong : MonoBehaviour
{
	public GameObject Earth;
	//public GameObject Sat;
	float degreesPerSecond;
	Rigidbody rb_earth;
	int frame = 0;
	float longitude()
	{
		Vector3 pos = transform.position;
		float l = (float)(Mathf.Atan2(pos.x, pos.z) * (180 / Mathf.PI));
		float rot_earth = rb_earth.rotation.eulerAngles.y;
		l = l - rot_earth;  // compute the azimuth angle with respect to earth axes
		if (frame % 100 == 0)
		{
			//print("pos: " + pos.ToString() + " long: " + l.ToString());
		}
		return l;
	}

	float latitude()
	{
		Vector3 pos = transform.position;
		float edist = Mathf.Sqrt(pos.x * pos.x + pos.z * pos.z);
		float lat = (float)(Mathf.Atan2(pos.y, edist) * (180 / Mathf.PI));
		//print("pos: " + pos.ToString() + " lat: " + lat.ToString());
		return lat;
	}

	float altitude()
	{
		Vector3 sat_pos = transform.position;
		float sat_distance = sat_pos.magnitude;
		float alt = sat_distance - GetSize().x;
		return alt;

	}

	Vector3 GetSize()
	{
		Mesh earth_mesh = Earth.GetComponentInChildren<MeshFilter>().mesh;
		Vector3 size = earth_mesh.bounds.size / 2;
		return size;
	}

	private void Start()
	{
		rb_earth = Earth.GetComponent<Rigidbody>();
	}

	private void Update()
	{
		float lat = latitude();
		float lon = longitude();
	}
}
