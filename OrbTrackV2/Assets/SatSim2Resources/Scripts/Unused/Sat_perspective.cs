﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sat_perspective : MonoBehaviour
{
    public GameObject Sat;

    // Update is called once per frame
    void Update()
    {
        Vector3 sat_pos = Sat.transform.position;
        transform.position = new Vector3(sat_pos.x, sat_pos.y, sat_pos.z);
        Vector3 cam_look = transform.forward;
        Vector3 radial_look_direction = -sat_pos;
        transform.forward = new Vector3(radial_look_direction.x, radial_look_direction.y, radial_look_direction.z);
    }
}
