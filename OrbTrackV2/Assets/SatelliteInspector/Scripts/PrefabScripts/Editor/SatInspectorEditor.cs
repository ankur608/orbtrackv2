﻿using UnityEngine;
using UnityEditor;
using System;
using SatelliteInspector.Scripts.PrefabScripts;
using SatelliteInspector.Scripts.SolutionScripts;

/// <summary>
/// Custom inspector for SatInspectorScript.cs
/// </summary>
[CustomEditor(typeof(SatInspectorScript))]
public class SatInspectorEditor : Editor
{
    private SatInspectorScript ins;

    #region Serialized Properties
    private SerializedProperty PInsTime;
    private SerializedProperty PTimeFactor;
    private SerializedProperty POrbitQual_SL;
    private SerializedProperty PisShowInfo;
    private SerializedProperty PisShowOrbit;
    private SerializedProperty PisShowVisor;
    //private SerializedProperty PisShowMag; //ADDED_14.6.23
    private SerializedProperty PTextSize;
    #endregion

    //===GameObjects===
    private PlaceScript PlaceSC;
    private EarthScript EarthSC;
    private SystemsScript SystemsSC;

    //===Editor variables===
    private Vector2 scrollPos;
    private Vector2 scrollPos2;
    private int CountSelectedSystems;
    private int UpdateIndex;
    private SatelliteScript[] AllSats;
    private bool FadeTime;

    private void OnEnable()
    {
        //SatInspectorScript being inspected
        ins = target as SatInspectorScript;
        //Init time parameters
        if (ins is null) return;
        ins.InsTime = DateTime.UtcNow.ToString("o");

        //Find properties in target
        PInsTime = serializedObject.FindProperty("InsTime");
        PTimeFactor = serializedObject.FindProperty("Timefactor");
        POrbitQual_SL = serializedObject.FindProperty("OrbitQual_SL");
        PisShowInfo = serializedObject.FindProperty("isShowInfo");
        PisShowOrbit = serializedObject.FindProperty("isShowOrbit");
        PisShowVisor = serializedObject.FindProperty("isShowVisor");
        //PisShowMag = serializedObject.FindProperty("isShowMag"); //ADDED_14.6.23
        PTextSize = serializedObject.FindProperty("TextSize");

        //Create and init Julian time
        ins.JTimeIns = CreateInstance<JTime>();
        ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));

        //Init EarthScript for gameobject "Earth" and set rotation
        EarthSC = ins.Earth.GetComponent<EarthScript>();
        EarthSC.RotateEarth();

        //Init PlaceScript for gameobject "Place" 
        PlaceSC = ins.Place.GetComponent<PlaceScript>();
        PlaceSC.LoadDicPlaces();
        PlaceSC.ShowName();

        //Init SystemsScript for gameobject "Systems"
        SystemsSC = ins.Systems.GetComponent<SystemsScript>();
        SystemsSC.GetNamesOfSystems();

        //Find all created SatellitesScripts (need onle for Editor)
        AllSats = ins.GetComponentsInChildren<SatelliteScript>();
    }

    public override void OnInspectorGUI()
    {
        //open if need show base "SatInspetorScript"
        //base.OnInspectorGUI();

        serializedObject.Update();

        #region Set time
        GUILayout.Box("Simulation time settings", EditorStyles.helpBox);
        EditorGUI.BeginChangeCheck();
        PInsTime.stringValue = EditorGUILayout.TextField("Time (full format)", PInsTime.stringValue);
        EditorGUILayout.LabelField("Time (standart format)", DateTime.Parse(PInsTime.stringValue).ToString());
        ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));
        #endregion

        #region Time control buttons
        FadeTime = EditorGUILayout.Foldout(FadeTime, "Time options");
        if (FadeTime)
        {
            if (GUILayout.Button("Set current time"))
            {
                PInsTime.stringValue = DateTime.UtcNow.ToString("o");
                ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));
                EarthSC.RotateEarth();
                UpdateAllSats();
            }

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("+ Day"))
            {
                PInsTime.stringValue = DateTime.Parse(PInsTime.stringValue).AddDays(1.0).ToString("o");
                ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));
                EarthSC.RotateEarth();
                UpdateAllSats();
            }
            if (GUILayout.Button("+ Hour"))
            {
                PInsTime.stringValue = DateTime.Parse(PInsTime.stringValue).AddHours(1.0).ToString("o");
                ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));
                EarthSC.RotateEarth();
                UpdateAllSats();
            }
            if (GUILayout.Button("+ Min"))
            {
                PInsTime.stringValue = DateTime.Parse(PInsTime.stringValue).AddMinutes(1.0).ToString("o");
                ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));
                EarthSC.RotateEarth();
                UpdateAllSats();
            }
            if (GUILayout.Button("+ Sec"))
            {
                PInsTime.stringValue = DateTime.Parse(PInsTime.stringValue).AddSeconds(1.0).ToString("o");
                ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));
                EarthSC.RotateEarth();
                UpdateAllSats();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("- Day"))
            {
                PInsTime.stringValue = DateTime.Parse(PInsTime.stringValue).AddDays(-1.0).ToString("o");
                ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));
                EarthSC.RotateEarth();
                UpdateAllSats();
            }
            if (GUILayout.Button("- Hour"))
            {
                PInsTime.stringValue = DateTime.Parse(PInsTime.stringValue).AddHours(-1.0).ToString("o");
                ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));
                EarthSC.RotateEarth();
                UpdateAllSats();
            }
            if (GUILayout.Button("- Min"))
            {
                PInsTime.stringValue = DateTime.Parse(PInsTime.stringValue).AddMinutes(-1.0).ToString("o");
                ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));
                EarthSC.RotateEarth();
                UpdateAllSats();
            }
            if (GUILayout.Button("- Sec"))
            {
                PInsTime.stringValue = DateTime.Parse(PInsTime.stringValue).AddSeconds(-1.0).ToString("o");
                ins.JTimeIns.InitAtTime(DateTime.Parse(PInsTime.stringValue));
                EarthSC.RotateEarth();
                UpdateAllSats();
            }
            EditorGUILayout.EndHorizontal();
        }
        if (Application.isPlaying)
        {
            PTimeFactor.floatValue = EditorGUILayout.Slider(PTimeFactor.floatValue, 1.0f, 60.0f);
        }
        #endregion

        #region Set place
        GUILayout.Box("Place settings", EditorStyles.helpBox);
        PlaceSC.Index = EditorGUILayout.Popup("Select Place", PlaceSC.Index, PlaceSC.NamesOfPlaces);
        PlaceSC.ShowName();
        EditorGUILayout.Vector3Field("LLA system position", PlaceSC.LLA(PlaceSC.Index));
        //EditorGUILayout.Vector3Field("ECI system position", PlaceSC.ECIposition(PlaceSC.LLA(PlaceSC.Index)));
        Vector3 PlaceUnityPos = PlaceSC.UnityPos(PlaceSC.ECIposition(PlaceSC.LLA(PlaceSC.Index)));
        PlaceSC.transform.localPosition = PlaceUnityPos;
        #endregion

        #region Create and delete systems
        GUILayout.Box("Add or remove satellites systems", EditorStyles.helpBox);
        EditorGUILayout.BeginHorizontal();

        SystemsSC.PopupSelectedIndex = EditorGUILayout.Popup(SystemsSC.PopupSelectedIndex, SystemsSC.NamesAllSystems);
        if (GUILayout.Button("Add"))
        {
            int indexOverwriteSystem = -1;
            foreach (GameObject createdsys in SystemsSC.ListCreatedSystems)
            {
                if (SystemsSC.NamesAllSystems[SystemsSC.PopupSelectedIndex] == createdsys.name)
                {
                    if (EditorUtility.DisplayDialog(
                        title: "Satellite system <" + createdsys.name + ">",
                        message: "This SatSystem already exists. Do you want to overwrite it?",
                        ok: "Overwrite",
                        cancel: "Create New"))
                    //OnOverWrite
                    {
                        indexOverwriteSystem = SystemsSC.ListCreatedSystems.IndexOf(createdsys);
                    }
                }
            }

            if (indexOverwriteSystem != -1)
            {
                SystemsSC.ListCreatedSystems.RemoveAt(indexOverwriteSystem);
                DestroyImmediate(SystemsSC.ListCreatedSystems[indexOverwriteSystem]);
            }

            GameObject system = Instantiate(SystemsSC.EmtySystem, SystemsSC.transform);
            system.name = SystemsSC.NamesAllSystems[SystemsSC.PopupSelectedIndex];
            SystemsSC.ListCreatedSystems.Add(system);
            SystemsSC.indexFromList = 0;
        }

        if (GUILayout.Button("Remove"))
        {
            SystemsSC.indexFromList = 0;
            for (int k = 0; k < CountSelectedSystems; k++)
            {
                for (int i = 0; i < SystemsSC.ListCreatedSystems.Count; i++)
                {
                    OneSystemScript onsys = SystemsSC.ListCreatedSystems[i].GetComponent<OneSystemScript>();
                    if (onsys.isChecked)
                    {
                        SystemsSC.ListCreatedSystems.RemoveAt(i);
                        DestroyImmediate(SystemsSC.ListCreatedSystems[i]);
                    }
                }
            }
        }
        EditorGUILayout.EndHorizontal();

        using (var h = new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
        {
            using (var scrollView = new EditorGUILayout.ScrollViewScope(scrollPos, GUILayout.Width(0), GUILayout.Height(0)))
            {
                scrollPos = scrollView.scrollPosition;
                CountSelectedSystems = 0;
                for (int i = 0; i < SystemsSC.ListCreatedSystems.Count; i++)
                {
                    OneSystemScript onsys = SystemsSC.ListCreatedSystems[i].GetComponent<OneSystemScript>();
                    onsys.isChecked = EditorGUILayout.ToggleLeft(onsys.name, onsys.isChecked);
                    if (onsys.isChecked)
                    {
                        CountSelectedSystems++;
                    }
                }
            }
        }
        #endregion

        #region Create Satellites
        GUILayout.Box("Add or remove satellites", EditorStyles.helpBox);
        EditorGUILayout.BeginHorizontal();
        SystemsSC.indexFromList = EditorGUILayout.Popup(SystemsSC.indexFromList, SystemsSC.NamesSystem);
        if (GUILayout.Button("Add"))
        {
            OneSystemScript subSystem = SystemsSC.SelectedSystem;
            if (subSystem != null)
            {
                subSystem.LoadMatrix();
                if (subSystem.SatellitesList.Count == 0)
                {
                    for (int i = 0; i < subSystem.CountSats; i++)
                    {
                        GameObject sat = Instantiate(subSystem.Satellite, subSystem.transform);

                        SatelliteScript satSC = sat.GetComponent<SatelliteScript>();
                        satSC.Init(subSystem, i);

                        sat.SetActive(false);
                        subSystem.SatellitesList.Add(sat);
                    }
                }
            }
        }
        if (GUILayout.Button("Remove"))
        {
            OneSystemScript subSystem = SystemsSC.SelectedSystem;
            subSystem.SatellitesList.Clear();
            if (subSystem != null)
            {
                SatelliteScript[] satScripts = subSystem.GetComponentsInChildren<SatelliteScript>(true);
                foreach (SatelliteScript satscript in satScripts)
                {
                    DestroyImmediate(satscript.gameObject);
                }
            }
        }
        EditorGUILayout.EndHorizontal();

        OneSystemScript SingleSystem = SystemsSC.SelectedSystem;
        if (SingleSystem != null)
        {
            EditorGUILayout.IntField("Count satellites", SingleSystem.CountSats, EditorStyles.label);

            using (var h = new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                using (var scrollView = new EditorGUILayout.ScrollViewScope(scrollPos2, GUILayout.Width(0), GUILayout.Height(0)))
                {
                    scrollPos2 = scrollView.scrollPosition;

                    for (int i = 0; i < SingleSystem.CountSats; i++)
                    {
                        if (SingleSystem.SatellitesList.Count != 0)
                        {
                            SatelliteScript SatScript = SingleSystem.SatellitesList[i].GetComponent<SatelliteScript>();
                            SatScript.isChecked = EditorGUILayout.ToggleLeft(SatScript.name, SatScript.isChecked);
                            if (SatScript.isChecked)
                            {
                                SatScript.gameObject.SetActive(true);
                            }
                            else SatScript.gameObject.SetActive(false);
                        }
                    }
                }
            }

            if (SingleSystem.SatellitesList.Count != 0)
            {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Show All"))
                {
                    for (int i = 0; i < SingleSystem.SatellitesList.Count; i++)
                    {
                        SingleSystem.SatellitesList[i].GetComponent<SatelliteScript>().isChecked = true;
                    }
                }
                if (GUILayout.Button("Hide All"))
                {
                    for (int i = 0; i < SingleSystem.SatellitesList.Count; i++)
                    {
                        SingleSystem.SatellitesList[i].GetComponent<SatelliteScript>().isChecked = false;
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
        }
        #endregion

        #region Satellites Settings
        //Satellite parameters settings
        EditorGUILayout.LabelField("Satellites parameters settings(PlayMode)", EditorStyles.helpBox);
        POrbitQual_SL.floatValue = EditorGUILayout.Slider("Vizual orbit quality", POrbitQual_SL.floatValue, 1.0f, 10.0f);
        PTextSize.intValue = EditorGUILayout.IntSlider("Font Size", PTextSize.intValue, 8, 28);
        PisShowInfo.boolValue = EditorGUILayout.Toggle("Show satellite info", PisShowInfo.boolValue);
        PisShowOrbit.boolValue = EditorGUILayout.Toggle("Show satellite orbit", PisShowOrbit.boolValue);
        PisShowVisor.boolValue = EditorGUILayout.Toggle("Show Satellite-link to Place", PisShowVisor.boolValue);
        //PisShowMag.boolValue = EditorGUILayout.Toggle("Show Magnetometer info", PisShowMag.boolValue); //ADDED_14.6.23
        #endregion

        #region Update satellites data
        if (!Application.isPlaying)
        {
            EditorGUILayout.LabelField("Sync Satellite Data from Celestrak", EditorStyles.helpBox);
            EditorGUILayout.BeginHorizontal();
            UpdateIndex = EditorGUILayout.Popup(UpdateIndex, SystemsSC.NamesAllSystems);
            if (GUILayout.Button("Update Specific"))
            {
                string updateFileName = SystemsSC.NamesAllSystems[UpdateIndex];
                ins.UpdateFile(updateFileName);
            }
            if (GUILayout.Button("Update All"))
            {
                foreach (string filename in SystemsSC.NamesAllSystems)
                {
                    ins.UpdateFile(filename);
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        #endregion

        serializedObject.ApplyModifiedProperties();
    }

    /// <summary>
    /// Update all satellite positions
    /// </summary>
    private void UpdateAllSats()
    {
        AllSats = ins.GetComponentsInChildren<SatelliteScript>();

        foreach (SatelliteScript sat in AllSats)
        {
            sat.FindNewPosition(DateTime.Parse(PInsTime.stringValue));
        }
    }
}

